function find(elements,callbackFb) {
    for (let i=0;i<=elements.length;i++){
        const currentValue=elements[i]
        const newValue=callbackFb(currentValue,i,elements);
        // console.log(newValue)
    }
}

module.exports=find;
